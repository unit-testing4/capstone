const { exchangeRates } = require('../src/util.js');
const express = require("express");
const e = require('express');
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})
	
	router.post('/currency', (req, res) => {

		let duplicate = exchangeRates.find((currency) => {

			return currency.alias === req.body.alias && currency.name === req.body.name
	
		});

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter NAME'
			})
		}

		if(req.body.name == ""){
			return res.status(400).send({
				'error' : 'Bad Request : NAME is empty'
			})
		}

		if(typeof(req.body.name) != "string"){
			return res.status(400).send({
				'error' : 'Bad Request : NAME should be a string'
			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter EX'
			})
		}

		if(Object.keys(req.body.ex).length == 0){
			return res.status(400).send({
				'error' : 'Bad Request : EX is empty'
			})
		}

		if(typeof(req.body.ex) != 'object'){
			return res.status(400).send({
				'error' : 'Bad Request : EX should be an object'
			})
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter ALIAS'
			})
		}

		if(req.body.alias == ""){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS is empty'
			})
		}

		if(typeof(req.body.alias) != "string"){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS should be a string'
			})
		}

		if(duplicate){
			return res.status(400).send({
				'error' : 'Bad Request : Duplicate currency was found'
			})
		}

		else{
			exchangeRates.push(req.body);
			return res.status(200).send(exchangeRates);
		}
	})

module.exports = router;